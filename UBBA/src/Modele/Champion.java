package Modele;

public class Champion {
	
	protected boolean isSelected = false;
	protected String nom;
	protected int pv, pvMax;
	protected int pa, paMax;
	protected int pm, pmMax;
	protected int posX, posY, posI=0, posJ=0;
	
	protected Champion(String s, int pvMax, int paMax, int pmMax)
	{
		posX = posY = posI = posJ = 0;
		nom = s;
		pv = this.pvMax = pvMax;
		pa = this.paMax = paMax;
		pm = this.pmMax = pmMax;

	}
	
	public int getPvMax() {
		return pvMax;
	}

	public void setPvMax(int pvMax) {
		this.pvMax = pvMax;
	}

	public int getPaMax() {
		return paMax;
	}

	public void setPaMax(int paMax) {
		this.paMax = paMax;
	}

	public int getPmMax() {
		return pmMax;
	}

	public void setPmMax(int pmMax) {
		this.pmMax = pmMax;
	}
	
	public void deplacer(int dX, int dY)
	{
		if (pm > 0)
		{
			boolean bouge = true;
			
			if (dX != 0)
				posX += dX;
			else if (dY != 0)
				posY += dY;
			else 
				bouge = false;
			
			if (bouge)
				pm--;
		}
	}

	//Package private
	void setPosX(int posX) {
		this.posX = posX;
	}

	void setPosY(int posY) {
		this.posY = posY;
	}

	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public int getPosI() {
		return posI;
	}

	public void setPosI(int posI) {
		this.posI = posI;
	}

	public int getPosJ() {
		return posJ;
	}

	public void setPosJ(int posJ) {
		this.posJ = posJ;
	}
	
	public String getNom() {
		return nom;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getPa() {
		return pa;
	}

	public void setPa(int pa) {
		this.pa = pa;
	}

	public int getPm() {
		return pm;
	}

	public void setPm(int pm) {
		this.pm = pm;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
}
