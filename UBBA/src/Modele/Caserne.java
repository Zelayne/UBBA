package Modele;

import java.lang.reflect.InvocationTargetException;

public class Caserne {

	public static Champion donnerChampion(String nom)
	{
		//Convertit la premiere lettre en capitale
		String className = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		
		try {
			Class<?> classe = Class.forName("Modele."+className);
			return (Champion) classe.getConstructor().newInstance();
		} catch (ClassNotFoundException e) {
			System.err.println(e);
			return null;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			System.err.println("Exception : ");
			e.printStackTrace();
			return null;
		}
	}
}
