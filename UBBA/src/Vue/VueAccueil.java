package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import Main.Ressources;

import javax.swing.JFrame;

import Modele.Jeu;

public class VueAccueil extends Vue implements ActionListener{
		
	private JPanel panelBoutons;
	private JButton jouer, hero, exit;
	private VueManager manager;
	
	public VueAccueil(VueManager m) {
		manager = m;
		
		setLayout(null);
		
		panelBoutons = new JPanel();
		panelBoutons.setBackground(Color.red);
		panelBoutons.setBounds(130, 820, 210, 170);
		
		jouer = new JButton("Jouer");
		jouer.setPreferredSize(new Dimension(200,50));
		
		hero = new JButton("Héros");
		hero.setPreferredSize(new Dimension(200,50));
		
		exit = new JButton("Quitter");
		exit.setPreferredSize(new Dimension(200,50));
		
		panelBoutons.add(jouer);
		panelBoutons.add(hero);
		panelBoutons.add(exit);
		
		add(panelBoutons);
		
		jouer.addActionListener(this);
		hero.addActionListener(this);
		exit.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		
		if (source == jouer) {
			manager.next();
		}
		else if (source == exit) {
			((JFrame) this.getTopLevelAncestor()).dispose() ; 
		}
	}
	
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(Ressources.getImage("acceuil"), 0, 0,null);
	}

	@Override
	public void clic(int x, int y, boolean enfonce) {}

	@Override
	public void survol(int x, int y, boolean enfonce) {}
}
