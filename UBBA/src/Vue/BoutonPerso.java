package Vue;

import java.awt.Color;
import java.awt.Graphics;

public class BoutonPerso {

	private int x, y;
	private boolean survol, sel;
	private int joueur;
	private static final int W = 110, H = 110;
	private static final Color COULEUR_SURVOL = Color.yellow;
	private static final Color[] COULEUR_SEL = {Color.blue, Color.red};
	
	public BoutonPerso(int x, int y)
	{
		this.x = x;
		this.y = y;
		survol = sel = false;
	}
	
	private boolean collision(int x2, int y2) {
		return x2 >= x && x2 <= x + W &&
				y2 >= y && y2 <= y + H;
	}
	
	public boolean survol(int x, int y)
	{
		return (survol = collision(x, y));
	}
	

	public boolean selection(int x, int y, int joueurSel)
	{
		if (collision(x, y))
		{
			joueur = joueurSel;
			return (sel = true);
		}
		
		return false;
	}
	
	public void afficher(Graphics g)
	{	
		if (sel)
			g.setColor(COULEUR_SEL[joueur - 1]);
		else if (survol)
			g.setColor(COULEUR_SURVOL);
		else
			g.setColor(Color.black);	
		
		g.fillRect(x, y, W, H);			
	}
	
}
