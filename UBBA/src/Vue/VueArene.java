package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JPanel;

import Main.Ressources;
import Modele.Jeu;
import Modele.Champion;


public class VueArene extends Vue implements Observer, ActionListener{
	
	int direction;
	
	private Jeu j;
	private JPanel panelBoutons;
	private JButton fintour;
	
	public VueArene(VueManager manager) {
		j = manager.getJeu();
		
		setLayout(null);
		
		panelBoutons = new JPanel();
		panelBoutons.setBackground(Color.yellow);
		panelBoutons.setBounds(1360, 920, 210, 60);
		
		fintour = new JButton("Fin du tour");
		fintour.setPreferredSize(new Dimension(200,50));
		
		panelBoutons.add(fintour);
		
		add(panelBoutons);
		
		fintour.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		
		if (source == fintour)
		{
			j.setTour(! j.getTour());
			repaint();
		}
		
		for (int i = 1; i <= 2; i++)
			j.getJoueur(i).setSelected(false);
	}
	
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
//|||||||||||||||||||||||||||||||||||||||||||>>>>>>-----REPRESENTATION DE LA PARTIE EN COURS--------<<<<<<||||||||||||||||||||||||||||||||||		
		
			
		int numJoueur = j.getTour() ? 2 : 1;
		Champion joueur = j.getJoueur(numJoueur);
		
		g.drawImage(Ressources.getImage("beau"), 0, 0, null);
		
		g.drawImage(Ressources.getImage("beau1"), 1025, 20, null);
		
//------------------------------------------DEPLACEMENT-------------------------------------------------------------
		if (joueur.isSelected()) {
			
			g.setColor(Color.orange);
			
			Coord[] cases = { 
					new Coord(-1, 0), 	//GAUCHE 
					new Coord(1, 0),	//DROIT
					new Coord(0, -1),	//HAUT
					new Coord(0, 1),	//BAS
			};
			
			for (Coord c : cases)
			{
				int x = c.x * 100 + joueur.getPosX();
				int y = c.y * 100 + joueur.getPosY();
				if (x >= 10 && x <= 910 && y >= 10 && y<= 910)
					g.fillRect(x, y, 100, 100);
			}
		}
		
		
		
//--------------------------------------------INTERFACE COMPETENCE--------------------------------------------------------------------------			
		
		g.setColor(Color.white);
		
		g.drawRect(1410, 50, 460, 450);
		g.drawRect(1055, 510, 815, 400);
		
		g.drawLine(1055, 710, 1870, 710);
		g.drawLine(1462,510, 1462, 910);
		
		g.drawRect(1160, 520, 290, 180);/*1*/
		g.drawRect(1567, 520, 290, 180);/*2*/
		g.drawRect(1160, 720, 290, 180);/*3*/
		g.drawRect(1567, 720, 290, 180);/*4*/
		
		g.setColor(Color.blue);
		g.fillRect(1070, 560, 75, 75);/*1*/
		g.fillRect(1477, 560, 75, 75);/*2*/
		g.fillRect(1070, 770, 75, 75);/*3*/
		g.fillRect(1477, 770, 75, 75);/*4*/
		
		
		g.setColor(Color.white);
		g.drawRect(1070, 560, 75, 75);/*1*/
		g.drawRect(1477, 560, 75, 75);/*2*/
		g.drawRect(1070, 770, 75, 75);/*3*/
		g.drawRect(1477, 770, 75, 75);/*4*/
		
		
//--------------------------------------------COMPETENCE1--------------------------------------------------------------------------
		/*
		if (dessine3 == 101) {
			g.setColor(Color.yellow);
			g.fillRect(j.getJoueur(1).getPosX()-10, j.getJoueur(1).getPosY()-10, 120, 120);
			
			
			g.setColor(Color.gray);
			g.fillRect(j.getJoueur(1).getPosX()-100, j.getJoueur(1).getPosY(), 90, 100);
			g.fillRect(j.getJoueur(1).getPosX()+110, j.getJoueur(1).getPosY(), 90, 100);
			g.fillRect(j.getJoueur(1).getPosX(), j.getJoueur(1).getPosY()-100, 100, 90);
			g.fillRect(j.getJoueur(1).getPosX(), j.getJoueur(1).getPosY()+110, 100, 90);
			
			
			
			g.setColor(Color.red);
			//GAUCHE
			if (direction==1) {
				
				g.fillRect(j.getJoueur(1).getPosX()-100, j.getJoueur(1).getPosY(), 90, 100);
			}	
			//DROITE
			if (direction==2) {
				
				g.fillRect(j.getJoueur(1).getPosX()+110, j.getJoueur(1).getPosY(), 90, 100);
			}	
			//HAUT
			if (direction==3) {
				
				g.fillRect(j.getJoueur(1).getPosX(), j.getJoueur(1).getPosY()-100, 100, 90);
			}
				
			//BAS
			if (direction==4) {
				
				g.fillRect(j.getJoueur(1).getPosX(), j.getJoueur(1).getPosY()+110, 100, 90);
			}
			
		}
		
		if (dessine3 == 201) {
			g.setColor(Color.yellow);
			g.fillRect(j.getJoueur(2).getPosX()-10, j.getJoueur(2).getPosY()-10, 120, 120);
			
			g.setColor(Color.gray);
			g.fillRect(j.getJoueur(2).getPosX()-100, j.getJoueur(2).getPosY(), 90, 100);
			g.fillRect(j.getJoueur(2).getPosX()+110, j.getJoueur(2).getPosY(), 90, 100);
			g.fillRect(j.getJoueur(2).getPosX(), j.getJoueur(2).getPosY()-100, 100, 90);
			g.fillRect(j.getJoueur(2).getPosX(), j.getJoueur(2).getPosY()+110, 100, 90);
			
			g.setColor(Color.red);
			//GAUCHE
			if (direction==1) {
				g.fillRect(j.getJoueur(2).getPosX()-100, j.getJoueur(2).getPosY(), 90, 100);
			}	
			//DROITE
			if (direction==2) {	
				g.fillRect(j.getJoueur(2).getPosX()+110, j.getJoueur(2).getPosY(), 90, 100);
			}	
			//HAUT
			if (direction==3) {
				g.fillRect(j.getJoueur(2).getPosX(), j.getJoueur(2).getPosY()-100, 100, 90);
			}
				
			//BAS
			if (direction==4) {
				g.fillRect(j.getJoueur(2).getPosX(), j.getJoueur(2).getPosY()+110, 100, 90);
			}
			
			
		}
		*/
//---------------------------------------------PERSONNAGES DANS ARENE--------------------------------------------------------------------------			
		
		for (int i = 1; i <= 2; i++)
		{
			Champion joueurCourant = j.getJoueur(i);
			String nomPerso = joueurCourant.getNom().toLowerCase();
			g.drawImage(Ressources.getImage("icone_" + nomPerso), joueurCourant.getPosX(), joueurCourant.getPosY(), null);
			
			if (i == numJoueur)
				g.drawImage(Ressources.getImage("stat_" + nomPerso), 1057, 50, null);
		}
		
//-------------------------------------------INTERFACE DES INFORMATIONS DES JOUEURS------------------------------------------------
		
		g.setColor(Color.white);
		g.drawRect(1025, 20, 875, 980);
		g.drawRect(1055, 50, 345, 450);
		
		
		g.setColor(Color.YELLOW);
		g.fillRect(1445, 135, 50, 30);
		g.fillRect(1445, 235, 50, 30);
		g.fillRect(1445, 335, 50, 30);
		
		g.setColor(Color.BLACK);
		g.drawRect(1445, 135, 50, 30);
		g.drawRect(1445, 235, 50, 30);
		g.drawRect(1445, 335, 50, 30);
		
		g.setFont(new Font("Helvetica", Font.PLAIN, 30));
		g.drawString("PV", 1450, 160);
		g.drawString("PA", 1450, 260);
		g.drawString("PM", 1450, 360);
		
		
		g.fillRect(1450, 50, 300, 60);
		g.setColor(Color.white);
		g.drawRect(1450, 50, 300, 60);
		
		g.setColor(Color.white);
		g.setFont(new Font("Alien Encounters", Font.PLAIN, 50));
		
		g.drawString(joueur.getNom(), 1520, 100);
		
		dessinerBarre(g, 1495, 135, joueur.getPv(), joueur.getPvMax(), Color.RED);
		dessinerBarre(g, 1495, 235, joueur.getPa(), joueur.getPaMax(), Color.ORANGE);
		dessinerBarre(g, 1495, 335, joueur.getPm(), joueur.getPmMax(), Color.GREEN);
		
		
//------------------------------------ARENE--------------------------------------------------
		int x = 10;
		int y = 10;
		for (int i=0;i<10;i++) {
			g.setColor(Color.white);
			g.drawRect(x, y, 100, 100);
			
			for (int j=0;j<10;j++) {
				g.drawRect(x, y, 100, 100);
				y += 100;
			}
			y = 10;
			x += 100;
		}		
	}
	
	private void dessinerBarre(Graphics g, int x, int y, int v, int max, Color couleur) {
		g.setColor(Color.BLACK);
		g.fillRect(x, y, max * 20 + 4, 32);
		g.setColor(couleur);
		g.fillRect(x + 2, y + 3, v * 20, 26);
		g.setColor(Color.white);
		g.drawString("" + v, 1450, y + 85);

		
	}

	public void update(Observable o, Object arg) {
		System.out.println("update");
		j = (Jeu) o;
        repaint();
		
	}

	public void clic(int x, int y, boolean enfonce) {
		
		int wCase = 100, hCase = 100;
		
		int joueurI = j.getTour() ? 2 : 1;
		int nbJoueurs = 2;
		Champion joueur = j.getJoueur(joueurI);
		
		if (! joueur.isSelected() && joueur.getPm() > 0 && enfonce)
		{
			//Deplacement
			boolean selected = x >= joueur.getPosX() &&
				x <= joueur.getPosX() + wCase && 
				y >= joueur.getPosY() && 
				y <= joueur.getPosY() + hCase;
	
			for (int i = 1; i <= nbJoueurs; i++)
			{
				j.getJoueur(i).setSelected(selected && i == joueurI);
			}
		}
		else if (joueur.isSelected() && !enfonce){
			int dx = (int)Math.floor((x - joueur.getPosX()) / 100f);
			int dy = (int)Math.floor((y - joueur.getPosY()) / 100f);
			
			if ((dx == 0 || dy == 0) && Math.abs(dx) <= 1 && Math.abs(dy) <= 1)
			{
				joueur.deplacer(dx * 100, dy * 100);	
				joueur.setSelected(false);
			}
			repaint();
		}
	
	
		if (x>=1075 && x<= 1150 && y>=550 && y<=625) {
			//Competences
			
		}
		repaint();	
	}

	public void survol(int x, int y, boolean enfonce) {
		
	}
	

}
